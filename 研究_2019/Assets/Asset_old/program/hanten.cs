﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hanten : MonoBehaviour {

	// Use this for initialization
	void Start () {

        //メッシュデータを調べる
        Mesh mesh = gameObject.GetComponent<MeshFilter>().mesh;
        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            print("vertices[" + i + "] : " + mesh.vertices[i]);
        }
        for (int i = 0; i < mesh.uv.Length; i++)
        {
            print("uv[" + i + "] : " + mesh.uv[i]);
        }
        for (int i = 0; i < mesh.triangles.Length; i++)
        {
            print("triangles[" + i + "] : " + mesh.triangles[i]);
        }

        //メッシュフィルターに変更したUVを入れる
        MeshFilter meshFilter = gameObject.GetComponent<MeshFilter>();
        Vector2[] newUV = new Vector2[9];

        /*そのまま貼付けた場合のUV
        newUV [0] = new Vector2(0.0f, 0.0f);
        newUV [1] = new Vector2(0.0f, 0.5f);
        newUV [2] = new Vector2(0.5f, 0.0f);
        newUV [3] = new Vector2(0.5f, 0.5f);
        newUV [4] = new Vector2(1.0f, 0.0f);
        newUV [5] = new Vector2(1.0f, 0.5f);
        newUV [6] = new Vector2(0.0f, 1.0f);
        newUV [7] = new Vector2(0.5f, 1.0f);
        newUV [8] = new Vector2(1.0f, 1.0f);
        */

        //左右対称にする。
        newUV[0] = new Vector2(1.0f, 0.0f);//左下を右下へ移動
        newUV[1] = new Vector2(1.0f, 0.5f);//左から右へ移動
        newUV[2] = new Vector2(0.5f, 0.0f);
        newUV[3] = new Vector2(0.5f, 0.5f);
        newUV[4] = new Vector2(1.0f, 0.0f);
        newUV[5] = new Vector2(1.0f, 0.5f);
        newUV[6] = new Vector2(1.0f, 1.0f);//左上から右上へ移動
        newUV[7] = new Vector2(0.5f, 1.0f);
        newUV[8] = new Vector2(1.0f, 1.0f);
        meshFilter.mesh.uv = newUV;

    }

    // Update is called once per frame
    void Update () {
		
	}
}
